module.exports = {
	root: true,
	env: {
		node: true,
		browser: true
	},
	plugins: ['vue', 'compat'],
	extends: ['plugin:vue/essential', '@vue/standard', 'prettier'],
	globals: {
		store: true,
		$store: true
	},
	rules: {
		'compat/compat': 'error',
		'no-tabs': 0,
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		eqeqeq: 'error',
		indent: [
			'error',
			'tab',
			{
				SwitchCase: 1
			}
		],
		'no-console': 'warn',
		'no-eval': 'error',
		quotes: ['error', 'single'],
		semi: [2, 'always'],
		strict: 'error',
		'array-bracket-spacing': ['error', 'never'],
		'object-curly-spacing': ['error', 'never'],
		'space-in-parens': ['error', 'never'],
		'space-before-function-paren': [
			'error',
			{
				anonymous: 'never',
				named: 'never'
			}
		],
		'space-before-blocks': 'error',
		'no-useless-escape': 0,
		'vue/attribute-hyphenation': ['error', 'always'],
		'vue/html-indent': ['error', 'tab']
	},
	parserOptions: {
		parser: 'babel-eslint'
	}
};

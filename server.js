const express = require('express');
const compress = require('compression');
const secure = require('express-force-https');
const path = require('path');
const serveStatic = require('serve-static');

let app = express();

// Use compression middleware
app.use(compress());
app.use(secure);
app.use(serveStatic(__dirname + '/dist'));

const port = process.env.PORT || 3000;
app.listen(port, () => {
	console.log('Listening on port ' + port);
});

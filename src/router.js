import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'home',
			component: () => import(/* webpackChunkName: "contact" */ './views/Home.vue'),
			meta: {
				overlayHeader: true
			}
		},
		{
			path: '/contact',
			name: 'contact',
			component: () => import(/* webpackChunkName: "contact" */ './views/Contact.vue'),
			meta: {
				overlayHeader: false
			}
		},
		{
			path: '*',
			name: '404',
			component: () => import(/* webpackChunkName: "404" */ './views/404.vue'),
			meta: {
				overlayHeader: false
			}
		}
	]
});

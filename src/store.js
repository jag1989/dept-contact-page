import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		contactQuestions: [
			{
				topic: 'Our Cheese',
				questions: [
					{
						question: 'How is it made?',
						answer:
							'The cheese needs to be refridgerated at a temperature between 2 and 5 degrees C, and should be consumed before the best before date.'
					},
					{
						question: 'How best to store it?',
						answer: 'In your fridge.'
					},
					{
						question: 'What is the shelf life?',
						answer: 'Lorem Ipsum.'
					},
					{
						question: 'Our wax',
						answer: 'Lorem Ipsum.'
					}
				]
			},
			{
				topic: 'Delivery',
				questions: [
					{
						question: 'Lorem Ipsum',
						answer: 'Lorem Ipsum'
					}
				]
			},
			{
				topic: 'Ordering',
				questions: [
					{
						question: 'Lorem Ipsum',
						answer: 'Lorem Ipsum'
					}
				]
			},
			{
				topic: 'Where can I buy',
				questions: [
					{
						question: 'Lorem Ipsum',
						answer: 'Lorem Ipsum'
					}
				]
			},
			{
				topic: 'Marketing',
				questions: [
					{
						question: 'Lorem Ipsum',
						answer: 'Lorem Ipsum'
					}
				]
			},
			{
				topic: 'Trade Enquiry',
				questions: [
					{
						question: 'Lorem Ipsum',
						answer: 'Lorem Ipsum'
					}
				]
			},
			{
				topic: 'Media Enquiry',
				questions: [
					{
						question: 'Lorem Ipsum',
						answer: 'Lorem Ipsum'
					}
				]
			}
		]
	},
	mutations: {},
	actions: {}
});

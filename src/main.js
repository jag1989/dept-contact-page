// Vue and Application
import Vue from 'vue';
import App from './App.vue';
// Router and Vuex Sync
import {sync} from 'vuex-router-sync';
// Router
import router from './router';
// Vuex Data Store
import store from './store';
// Bootstrap
import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

Vue.config.productionTip = false;

// eslint-disable-next-line no-unused-vars
const unsync = sync(store, router);

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');

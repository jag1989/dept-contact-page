const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = {
	productionSourceMap: false,
	css: {
		sourceMap: true
	},
	lintOnSave: true,
	configureWebpack: {
		entry: ['@babel/polyfill', './src/main.js'],
		plugins: [
			new StyleLintPlugin({
				files: ['src/**/*.vue', 'src/**/*.scss']
			})
		]
	},
	pluginOptions: {
		lintStyleOnBuild: false,
		stylelint: {
			fix: true
		}
	},
	chainWebpack: config => {
		config.module
			.rule('vue')
			.use('vue-svg-inline-loader')
			.loader('vue-svg-inline-loader');
	}
};
